
var device = argument0;
if compat_gamepad_required() {
    // Not supported in Joy DLL yet
    return 0;
}
else return gamepad_get_button_threshold(device);
