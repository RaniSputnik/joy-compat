
var device = argument0;
var deadzone = argument1;
if compat_gamepad_required() {
    // Currently no alternative in Joy DLL
    return 0;
}
else return gamepad_set_axis_deadzone(device,deadzone);
