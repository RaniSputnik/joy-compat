
var device = argument0;
var buttonIndex = argument1;
if compat_gamepad_required() {
    with objCompatGamepad {
        buttonIndex = compat_gamepad_button_index(buttonIndex);
        return gp_button_check[device,buttonIndex];
    }
}
else return gamepad_button_value(device,buttonIndex);
