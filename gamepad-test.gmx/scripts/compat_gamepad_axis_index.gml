
var axis = argument0;
switch axis {
    case gp_axislh: return 0;
    case gp_axislv: return 1;
    // 2 = the bottom triggers
    case gp_axisrv: return 3;
    case gp_axisrh: return 4;
}
return axis;
