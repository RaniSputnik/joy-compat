
var device = argument0;
if compat_gamepad_required() {
    return joy_count() > device;
}
else return gamepad_is_connected(device);
