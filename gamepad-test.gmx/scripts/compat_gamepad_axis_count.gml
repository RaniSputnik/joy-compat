
var device = argument0;
if compat_gamepad_required() {
    return joy_axes(device);
}
else return gamepad_axis_count(device)
