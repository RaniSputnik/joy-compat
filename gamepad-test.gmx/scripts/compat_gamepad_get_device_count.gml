
if compat_gamepad_required() {
    var c = joy_count();
    if c != 0 return c;
    else return gamepad_get_device_count();
}
else return gamepad_get_device_count();
