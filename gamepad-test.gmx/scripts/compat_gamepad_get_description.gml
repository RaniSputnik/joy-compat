
var device = argument0;
if compat_gamepad_required() {
    return joy_name(device);
}
else return gamepad_get_description(device);
