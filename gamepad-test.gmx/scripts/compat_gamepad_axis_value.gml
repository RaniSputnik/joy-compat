
var device = argument0;
var axisIndex = argument1;
if compat_gamepad_required() {
    axisIndex = compat_gamepad_axis_index(axisIndex);
    return joy_axis(device,axisIndex);
}
else return gamepad_axis_value(device,axisIndex);
