
var button = argument0;
switch button {
    case gp_face1: return 0;
    case gp_face2: return 1;
    case gp_face3: return 2;
    case gp_face4: return 3;
    case gp_shoulderl: return 4;
    case gp_shoulderr: return 5;
    case gp_select: return 6;
    case gp_start: return 7;
    case gp_stickl: return 8;
    case gp_stickr: return 9;
    case gp_padu: return 10;
    case gp_padd: return 11;
    case gp_padl: return 12;
    case gp_padr: return 13;
}

return button;

