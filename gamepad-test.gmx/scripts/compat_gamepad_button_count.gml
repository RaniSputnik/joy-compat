
var device = argument0;
if compat_gamepad_required() {
    return joy_buttons(device) + joy_hats(device)*4;
}
else return gamepad_button_count(device);
